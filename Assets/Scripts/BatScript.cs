﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatScript : MonoBehaviour {

    public int life;
    private CameraScript cam;
    public string enemyMap;
    private GameObject player;
    private Rigidbody2D rig;
    private Animator ani;
    public float speed;
    public AudioClip sound, batHit;
    private AudioSource audioSource;
    private float audioTimer;


    void Start()
    {
        cam = GameObject.Find("Camera").GetComponent<CameraScript>();
        player = GameObject.Find("Player");
        rig = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    void PlaySound()
    {
        audioSource.clip = sound;
        audioSource.Play();
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (ani.GetBool("Dead"))
            return;
        if (life <= 0)
            die();
        if (cam.GetCurrentMap().tag == enemyMap)
            Move();
        else
            rig.velocity = new Vector2();
    }

    void Move()
    {
        Vector3 p = player.transform.position;
        Vector3 e = transform.position;
        Vector3 t = p - e;
        rig.velocity = new Vector2(t.x, t.y).normalized * speed;
        audioTimer += Time.deltaTime;
        if (audioTimer >= Random.Range(2f, 7f))
        {
            audioTimer = 0;
            PlaySound();
        }

    }

    public void Hit()
    {
        Debug.Log(life);
        life -= 50;
        audioSource.clip = batHit;
        audioSource.Play();
    }

    void die()
    {
        ani.SetBool("Dead", true);
        Destroy(rig);
        Destroy(GetComponent<PolygonCollider2D>());
        //Invoke("WakeUp", 10f);
    }

    private void WakeUp()
    {
        ani.SetBool("Dead", false);
        life = 50;
        GetComponent<PolygonCollider2D>().enabled = false;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player" && !ani.GetBool("Dead"))
        {
            collision.gameObject.GetComponent<PlayerController>().Hit(5);
        }
    }
}
