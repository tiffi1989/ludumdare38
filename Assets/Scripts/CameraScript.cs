﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    private GameObject currentMap;
    private Transform player;
    private float left;
    private float right;
    private float top;
    private float bottom;
    private Camera cam;
    private float camHeight;
    private float camWidth;
    private float shakeTimer;
    private float shakeAmount = 0.1f;
    private float decreaseFactor = 1.0f;

	// Use this for initialization
	void Start () {
        this.player = GameObject.Find("Player").GetComponent<Transform>();
        this.cam = GetComponent<Camera>();
        this.camHeight = 2f * cam.orthographicSize;
        this.camWidth = camHeight * cam.aspect;
        SetUpNewLevel(GameObject.Find("MapTopRight"));
    }

    public void SetUpNewLevel(GameObject newMap)
    {
        this.currentMap = newMap;
        left = currentMap.transform.position.x - (currentMap.GetComponent<SpriteRenderer>().size.x * currentMap.transform.localScale.x) / 2;
        right = currentMap.transform.position.x + (currentMap.GetComponent<SpriteRenderer>().size.x * currentMap.transform.localScale.x) / 2;
        top = currentMap.transform.position.y + (currentMap.GetComponent<SpriteRenderer>().size.y * currentMap.transform.localScale.y) / 2;
        bottom = currentMap.transform.position.y - (currentMap.GetComponent<SpriteRenderer>().size.y * currentMap.transform.localScale.y) / 2;
        Debug.Log(currentMap.GetComponent<SpriteRenderer>().size.y + " " + currentMap.transform.localScale.y);
    }

    // Update is called once per frame
    void Update () {

        if(player.position.x - camWidth/2 > left && player.position.x + camWidth/2 < right)
        {
            //Debug.Log("x");
            this.transform.position = new Vector3(player.position.x, this.transform.position.y, -10);
        }
        if(player.position.y - camHeight / 2 > bottom && player.position.y + camHeight / 2 < top)
        {
            //Debug.Log("y");
            this.transform.position = new Vector3(this.transform.position.x, player.position.y, -10);
        }
        if (shakeTimer > 0)
        {
            cam.transform.localPosition = new Vector3(cam.transform.localPosition.x + Random.insideUnitCircle.x * shakeAmount, cam.transform.localPosition.y + Random.insideUnitCircle.y * shakeAmount, cam.transform.localPosition.z);
            shakeTimer -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeTimer = 0;
        }

    }

    public GameObject GetCurrentMap()
    {
        return this.currentMap;
    }

    public void setShakeTime(float time)
    {
        Debug.Log("ShakeTimer set to " + time + " Seconds");
        shakeTimer = time;
    }
}
