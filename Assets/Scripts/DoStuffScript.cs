﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoStuffScript : MonoBehaviour {

    private Inventory inventory;
    public GameObject bridge;
    public GameObject fireFly;
    private Light playerLight;
    private ArrayList currentTrigger;
    public GameObject darkness;
    public GameObject[] activateAfterDarkness;
    public GameObject bigStone;
    public GameObject end;
    public GameObject dark;
    private bool endTrigger, firstTimeTriggered;
    

	// Use this for initialization
	void Start () {
        this.inventory = GetComponent<Inventory>();
        this.playerLight = GetComponentInChildren<Light>();
        currentTrigger = new ArrayList();
	}
	
	// Update is called once per frame
	void Update () {
        if(currentTrigger.Contains("pressButtonBridge") && Input.GetButton("Jump") && inventory.hasPlayerItem("log"))
        {
            GameObject o = GameObject.Find("BaumTrigger");
            bridge.GetComponent<BoxCollider2D>().enabled = false;
            bridge.GetComponent<SpriteRenderer>().enabled = true;
            o.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            inventory.RemoveItem("log");
            Destroy(o.gameObject);
        }

        if (currentTrigger.Contains("pressButtonFireFly") && Input.GetButton("Jump") && inventory.hasPlayerItem("jar"))
        {
            GameObject o = GameObject.Find("PondTextTrigger");
            Destroy(fireFly);
            o.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            inventory.RemoveItem("jar");
            inventory.addItem("lightjar");
            playerLight.enabled = true;
            Destroy(o.gameObject);
            Destroy(darkness);
            foreach(GameObject g in activateAfterDarkness)
            {
                g.SetActive(true);
            }
        }
        if (currentTrigger.Contains("pressButtonFire") && Input.GetButton("Jump") && inventory.hasPlayerItem("firestone"))
        {
            GameObject o = GameObject.Find("FirePlaceTrigger");
            o.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            GameObject b = GameObject.Find("fireplace");
            b.GetComponent<Animator>().SetBool("isBurning", true);
            inventory.RemoveItem("firestone");
            inventory.addItem("coal");
            Destroy(o.gameObject);
        }
        if (currentTrigger.Contains("pressButtonStone") && Input.GetButton("Jump") && inventory.hasPlayerItem("dynamite"))
        {
            GameObject o = GameObject.Find("LargeStoneTextTrigger");
            o.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            bigStone.GetComponent<Animator>().SetBool("explode", true);
            bigStone.GetComponent<BoxCollider2D>().enabled = false;
            inventory.RemoveItem("coal");
            Invoke("removeSulfur", 2);
            Destroy(o.gameObject);
        }
        if (currentTrigger.Contains("pressButtonHang") && Input.GetButton("Jump") && inventory.hasPlayerItem("rope"))
        {

            GameObject o = GameObject.Find("HangTreeTrigger");
            o.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            Debug.Log("END");
            endTrigger = true;           

        }

        if (endTrigger && !firstTimeTriggered)
        {
            firstTimeTriggered = true;
            GameObject.Find("Player").GetComponent<PlayerController>().SetEnd();
            InvokeRepeating("fadeOut", .01f, .05f);
        }
    }

    private void fadeIn()
    {
        dark.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, dark.GetComponent<SpriteRenderer>().color.a -0.01f);

        if(dark.GetComponent<SpriteRenderer>().color.a <= 0f)
        {
            GameObject.Find("Player").GetComponent<TalkScript>().SetText("It's not too bad afterall :)");
            GameObject.Find("TopText").GetComponent<Text>().color = new Color(0, 0, 0, GameObject.Find("TopText").GetComponent<Text>().color.a + 0.03f);
            
        } else
        {
            GameObject.Find("TopText").GetComponent<Text>().color = new Color(0, 0, 0, 0);
            GameObject.Find("TopText").GetComponent<TextFadeOut>().enabled = false;
        }

    }

    private void fadeOut()
    {

        Debug.Log(dark.GetComponent<SpriteRenderer>().color.a);
        dark.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, dark.GetComponent<SpriteRenderer>().color.a + 0.01f);

        if(dark.GetComponent<SpriteRenderer>().color.a >= 1f)
        {
            GameObject o = GameObject.Find("Player");
            o.gameObject.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            CancelInvoke();
            GameObject.Find("Camera").transform.position = new Vector3(end.transform.position.x, end.transform.position.y-2, -10);
            end.SetActive(true);

            InvokeRepeating("fadeIn", .01f, .05f);


        }
    }



    private void removeSulfur()
    {
        inventory.RemoveItem("sulfur");

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        currentTrigger.Add(collision.tag);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        currentTrigger.Remove(collision.tag);
    }
}
