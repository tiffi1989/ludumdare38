﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    private int life = 400;
    private CameraScript cam;
    private SpriteRenderer renderer;
    public string enemyMap;
    private GameObject player;
    private Rigidbody2D rig;
    public float speed;
    private bool attacking, playerInRange, dmgDone;
    private Animator ani;
    private float attackTimer, dmgTimer, blinkTimer;
    public int dmg;
    public GameObject jar;
    private int blink = 10;
    public AudioClip deathSound, hitSound;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        cam = GameObject.Find("Camera").GetComponent<CameraScript>();
        player = GameObject.Find("Player");
        rig = GetComponent<Rigidbody2D>();
        ani = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (ani.GetBool("Dead"))
            return;
        if (life <= 0)
            die();
        blinkTimer += Time.deltaTime;

        if (blink < 7 && blinkTimer > .1f)
        {
            blink++;
            renderer.color = renderer.color == Color.red ? Color.white : Color.red;
            blinkTimer = 0;
        }
        if(blink == 7)
        {
            renderer.color = Color.white;
        }
        if (cam.GetCurrentMap().tag == enemyMap && !playerInRange && !attacking)
            Move();
        else
            rig.velocity = new Vector2();

        if (attacking)
        {
            Attack();
        }
	}

    public void Move()
    {
        Vector3 p = player.transform.position;
        Vector3 e = transform.position;
        Vector3 t = p - e;
        rig.velocity = new Vector2(t.x, t.y).normalized * speed;
        if (rig.velocity.x > 0)
            transform.localScale = new Vector3(transform.localScale.y, transform.localScale.y, transform.localScale.z);
        else transform.localScale = new Vector3(-transform.localScale.y, transform.localScale.y, transform.localScale.z);
    }

    public void Hit()
    {
        life -= 50;
        blink = 0;
        source.clip = hitSound;
        source.Play();
    }

    public void Attack()
    {
        dmgTimer += Time.deltaTime;
        attackTimer += Time.deltaTime;

        if(dmgTimer > .8f)
        {
            dmgTimer = 0;
            cam.setShakeTime(.3f);
            source.clip = hitSound;
            source.Play();

            if (playerInRange)
            {
                Debug.Log("Troll hit Player");
                player.GetComponent<PlayerController>().Hit(dmg);
            }
        }
        if(attackTimer > 1.5f)
        {
            dmgTimer = 0;
            Invoke("StopAttack", 1f);
        }
    }

    void StopAttack()
    {
        attackTimer = 0;
        attacking = false;
        if (playerInRange)
            ani.ForceStateNormalizedTime(0);
        else
            ani.SetBool("Attacking", false);
        dmgTimer = 0;
    }

    private void die()
    {
        ani.SetBool("Dead", true);
        Invoke("WakeUp", 20f);
        renderer.color = Color.white;
        blink = 10;
        source.clip = hitSound;
        source.Play();

        if (!player.GetComponent<Inventory>().hasPlayerItem("jar") && !player.GetComponent<Inventory>().hasPlayerItem("lightjar"))
        {
            GameObject jarClone = Instantiate(jar, transform.position, Quaternion.identity);
            jarClone.GetComponent<Rigidbody2D>().velocity = Random.insideUnitCircle.normalized * 10;
        }
    }

    private void WakeUp()
    {
        ani.SetBool("Dead", false);
        attacking = false;
        life = 300;
        attackTimer = 0;
        dmgTimer = 0;
        ani.SetBool("Attacking", false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            playerInRange = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !attacking)
        {
            attacking = true;
            ani.SetBool("Attacking", true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            playerInRange = false;
        }
    }

}
