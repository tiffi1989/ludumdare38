﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFlyScript : MonoBehaviour {

    private Vector3 startPosition;
    private float timer = 5;
    private Vector3 targetPosition;
    private float radius = 1;
    private float speed = .25f;

	// Use this for initialization
	void Start () {
        this.startPosition = transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        timer += Time.deltaTime;
		if(timer < Random.Range(2f, 3.5f))
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        } else
        {
            timer = 0;
            float t = 2 * Mathf.PI * Random.value;
            float u = Random.value + Random.value;
            float r = u > 1 ? 2 - u : u;
            r = r * radius;
            targetPosition = new Vector3(r * Mathf.Cos(t) + startPosition.x, r * Mathf.Sin(t) + startPosition.y, startPosition.z);
        }
	}
}
