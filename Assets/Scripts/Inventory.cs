﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    private ArrayList items;
    private TalkScript talk;
    private TextFadeOut inventoryText;

	// Use this for initialization
	void Start () {
        this.talk = GetComponent<TalkScript>();
        this.items = new ArrayList();
        this.inventoryText = GameObject.Find("InventoryText").GetComponent<TextFadeOut>();
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("i"))
        {
            string wholeString = "Inventory:\n";
            foreach(string s in items)
            {
                wholeString += s + "\n";
            }
            inventoryText.SetText(wholeString);
        }
	}

    public void addItem(string item)
    {
        items.Add(item);
        talk.SetItemText(item);
        talk.SetSpecialTextIfAvailable(item);

        if (items.Contains("coal") && items.Contains("sulfur"))
        {
            items.Add("dynamite");
            Invoke("thinkAboutDynamite", 10);
        }

    }


    public void thinkAboutDynamite()
    {
        this.talk.SetSpecialText("thinking");
    }

    public void RemoveItem(string item)
    {
        items.Remove(item);
        talk.SetItemRemovedText(item);
    }

    public bool hasPlayerItem(string item)
    {
        return items.Contains(item);
    }
}
