﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour {
    private float timer;
    public string item;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            timer += Time.deltaTime;

            if (timer > .2f)
            {
                collision.gameObject.GetComponent<Inventory>().addItem(item);
                collision.gameObject.GetComponent<TalkScript>().SetSpecialTextIfAvailable(item);
                Destroy(gameObject);
            }
        }
    }
}
