﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JarScript : MonoBehaviour {

    private float timer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            timer += Time.deltaTime;

            if(timer > .6f)
            {
                collision.gameObject.GetComponent<Inventory>().addItem("jar");
                collision.gameObject.GetComponent<TalkScript>().SetSpecialText("jar");
                Destroy(gameObject);
            }
        }
    }
}
