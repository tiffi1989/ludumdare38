﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeTextSctipt : MonoBehaviour {

    private PlayerController player;
    private Text text;

	// Use this for initialization
	void Start () {
        this.player = GameObject.Find("Player").GetComponent<PlayerController>();
        this.text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        this.text.text = "H:" + player.getLife();
	}
}
