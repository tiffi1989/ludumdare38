﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    private Rigidbody2D rig;
    public int speed;
    private Animator ani;
    private bool deactivateControls;
    private SpriteRenderer weaponRenderer;
    public CircleCollider2D weaponCollider;
    public Transform weaponTransform;
    public Rigidbody2D weaponRig;
    private float attackTimer;
    private int life = 100;
    private Camera cam;
    private bool hasWeapon;
    private TalkScript talk;
    private bool dead, gameStarted;
    private float timeSinceLastHitTimer;
    private GameObject startDark;
    public GameObject lifeText, StartText, StartText2;
    private AudioSource source;
    private bool end;

    // Use this for initialization
    void Start()
    {
        this.rig = GetComponent<Rigidbody2D>();
        this.ani = GetComponent<Animator>();
        this.cam = GameObject.Find("Camera").GetComponent<Camera>();
        this.talk = GetComponent<TalkScript>();
        this.startDark = GameObject.Find("StartDark");
        this.source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!gameStarted && Input.GetButton("Jump"))
        {
            gameStarted = true;
        }

        if (dead || !gameStarted || end)
            return;

        Debug.Log(startDark.GetComponent<SpriteRenderer>().color.a);
        if(startDark.GetComponent<SpriteRenderer>().color.a > 0)
        {
            startDark.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, startDark.GetComponent<SpriteRenderer>().color.a - .01f);
            StartText.GetComponent<Text>().color = new Color(0, 0, 0, startDark.GetComponent<SpriteRenderer>().color.a - .01f);
            StartText2.GetComponent<Text>().color = new Color(0, 0, 0, startDark.GetComponent<SpriteRenderer>().color.a - .01f);

            return;
        } else
        {
            GetComponent<CircleCollider2D>().enabled = true;
            lifeText.SetActive(true);
            

        }
        attackTimer += Time.deltaTime;
        timeSinceLastHitTimer += Time.deltaTime;
        if(timeSinceLastHitTimer > 10f)
        {
            life += life <= 100 ? 1 : 0;
            timeSinceLastHitTimer = 8;
        }
        if (!deactivateControls)
        {
            Movement();
            if (Input.GetButtonDown("Fire1") && attackTimer > .7f && hasWeapon)
                Attack();
        }
        else
            rig.velocity = new Vector3();
        ControllAnimator();

        if (life <= 0)
            Die();

    }

    void Movement()
    {
        Vector2 dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        rig.velocity = dir * speed;
    }

    void Attack()
    {
        attackTimer = 0;
        ani.SetBool("attack", true);
        Invoke("StartAttack", .2f);
    }

    void StartAttack()
    {
        
        weaponCollider.enabled = true;
        weaponRig.AddForce(Vector2.up * Time.deltaTime * 0.0000000001f);
        if (ani.GetBool("looksUp"))
        {
            weaponTransform.localPosition = new Vector3(0, .2f, weaponTransform.localPosition.z);
        }
        if (ani.GetBool("looksDown"))
        {
            weaponTransform.localPosition = new Vector3(0, -.2f, weaponTransform.localPosition.z);
        }
        if (ani.GetBool("looksRightOrLeft"))
        {
            weaponTransform.localPosition = new Vector3(-.2f, 0, weaponTransform.localPosition.z);
        }
        Invoke("StopAttack", .5f);
        source.Play();

    }

    void StopAttack()
    {
        weaponCollider.enabled = false;
        ani.SetBool("attack", false);
    }

    void ControllAnimator()
    {
        if (rig.velocity.x != 0 || rig.velocity.y != 0)
        {
            ani.SetBool("isWalking", true);
            if (rig.velocity.x != 0)
            {
                ani.SetBool("looksRightOrLeft", true);
                ani.SetBool("looksUp", false);
                ani.SetBool("looksDown", false);
                if (rig.velocity.x < 0)
                    transform.localScale = new Vector3(3, transform.localScale.y, transform.localScale.z);
                if (rig.velocity.x > 0)
                    transform.localScale = new Vector3(-3, transform.localScale.y, transform.localScale.z);
            }
            else
            {
                ani.SetBool("looksRightOrLeft", false);
                if (rig.velocity.y > 0)
                {
                    ani.SetBool("looksUp", true);
                    ani.SetBool("looksDown", false);
                }
                if (rig.velocity.y < 0)
                {
                    ani.SetBool("looksUp", false);
                    ani.SetBool("looksDown", true);
                }

            }
        }
        else
        {
            ani.SetBool("isWalking", false);
        }

    }

    public void SetDeactivateControls(bool b)
    {
        this.deactivateControls = b;
    }

    public void Hit(int dmg)
    {
        life -= dmg;
        timeSinceLastHitTimer = 0;
    }

    private void Die()
    {
        talk.SetSpecialText("dead");
        ani.SetBool("dead", true);
        ani.SetBool("looksRightOrLeft", false);
        ani.SetBool("looksUp", false);
        ani.SetBool("looksDown", false);
        ani.SetBool("attack", false);
        Destroy(rig);
        dead = true;
        Invoke("Restart", 3f);
    }

    private void Restart()
    {
        SceneManager.LoadScene("1");
    }

    public int getLife()
    {
        return life;
    }

    public void SetEnd()
    {
        end = true;
    }

    public void EnableWeapon()
    {
        hasWeapon = true;
        talk.SetSpecialText("weapon");
        talk.SetItemText("weapon");
    }
}
