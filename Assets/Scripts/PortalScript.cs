﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction {top, bottom, left, right };

public class PortalScript : MonoBehaviour {

    public bool top_bottom;
    private PlayerController playerController;
    private GameObject cam;
    private bool animationRunning;
    private float camStartX;
    private float camStartY;
    private float camHeight;
    private float camWidth;
    private CameraScript camScript;
    private GameObject player;
    private Direction dir;

    // Use this for initialization
    void Start () {
        this.playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        this.cam = GameObject.Find("Camera");
        this.camHeight = 2f * cam.GetComponent<Camera>().orthographicSize;
        this.camWidth = camHeight * cam.GetComponent<Camera>().aspect;
        this.camScript = cam.GetComponent<CameraScript>();
        this.player = GameObject.Find("Player");
    }
	
	// Update is called once per frame
	void Update () {
        if (this.animationRunning)
        {
            switch (dir)
            {
                case Direction.top:
                    SlideVertical(true);
                    break;
                case Direction.bottom:
                    SlideVertical(false);
                    break;
                case Direction.right:
                    SlideHorizontal(true);
                    break;
                case Direction.left:
                    SlideHorizontal(false);
                    break;
            }
        }
	}

    private void SlideVertical(bool top)
    {
        if((top && this.cam.transform.position.y < camStartY + camHeight) || (!top && this.cam.transform.position.y > camStartY - camHeight))
        {
            this.cam.transform.position = new Vector3(this.cam.transform.position.x, this.cam.transform.position.y + (top ? 0.1f : -0.1f), this.cam.transform.position.z);
            player.transform.position = new Vector3(this.player.transform.position.x, this.player.transform.position.y + (top ? 0.03f : -0.03f), this.player.transform.position.z);
        } else
        {
            EndAni();
            if (top)
            {
                if (this.camScript.GetCurrentMap().tag == "MapBottomRight")
                    this.camScript.SetUpNewLevel(GameObject.Find("MapTopRight"));
                else
                    this.camScript.SetUpNewLevel(GameObject.Find("MapTopLeft"));
            } else
            {
                if (this.camScript.GetCurrentMap().tag == "MapTopRight")
                    this.camScript.SetUpNewLevel(GameObject.Find("MapBottomRight"));
                else
                    this.camScript.SetUpNewLevel(GameObject.Find("MapBottomLeft"));
            }
        }
    }

    private void SlideHorizontal(bool right)
    {
        if ((right && this.cam.transform.position.x < camStartX + camWidth) || (!right && this.cam.transform.position.x > camStartX - camWidth))
        {
            this.cam.transform.position = new Vector3(this.cam.transform.position.x + (right ? 0.1f : -0.1f), this.cam.transform.position.y, this.cam.transform.position.z);
            player.transform.position = new Vector3(this.player.transform.position.x + (right ? 0.013f : -0.013f), this.player.transform.position.y, this.player.transform.position.z);

        }
        else
        {
            
            if (right)
            {
                if (this.camScript.GetCurrentMap().tag == "MapTopLeft")
                    this.camScript.SetUpNewLevel(GameObject.Find("MapTopRight"));
                else
                    this.camScript.SetUpNewLevel(GameObject.Find("MapBottomRight"));
            }
            else
            {
                if (this.camScript.GetCurrentMap().tag == "MapTopRight")
                    this.camScript.SetUpNewLevel(GameObject.Find("MapTopLeft"));
                else
                    this.camScript.SetUpNewLevel(GameObject.Find("MapBottomLeft"));
            }
            EndAni();
        }
    }

    private void EndAni()
    {
        this.playerController.SetDeactivateControls(false);
        this.animationRunning = false;
        //Debug.Log("Portal Animation Done, current Map is now: " + this.camScript.GetCurrentMap().tag);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player" && !this.animationRunning)
        {
            this.playerController.SetDeactivateControls(true);
            this.animationRunning = true;
            this.camStartX = this.cam.transform.position.x;
            this.camStartY = this.cam.transform.position.y;

            if (top_bottom)
            {
                if (player.transform.position.y < transform.position.y)
                    dir = Direction.top;
                else
                    dir = Direction.bottom;
            }
            else
            {
                if (player.transform.position.x < transform.position.x)
                    dir = Direction.right;
                else
                    dir = Direction.left;
            }
            //Debug.Log("Portal activated from " + this.camScript.GetCurrentMap().tag + " in Direction " + dir);
        }
    }
}
