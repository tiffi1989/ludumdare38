﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScullBallScript : MonoBehaviour {

    private Rigidbody2D rig;
    public Animator ani;
    public float speed;

	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Weapon")
        {
            Debug.Log("Skull Hit");
            rig.freezeRotation = false;
            rig.constraints = RigidbodyConstraints2D.None;
            Vector3 playerPos = collision.gameObject.GetComponent<Transform>().position;
            Vector3 skullPos = rig.position;
            if(ani.GetBool("looksUp"))
                rig.AddForce(Vector2.up * speed);
            if (ani.GetBool("looksDown"))
                rig.AddForce(Vector2.down * speed);
            if (ani.GetBool("looksRightOrLeft"))
            {
                if(playerPos.x < skullPos.x)
                    rig.AddForce(Vector2.right * speed);
                else
                    rig.AddForce(Vector2.left * speed);

            }

            rig.AddTorque(40);


        }
    }

}
