﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkScript : MonoBehaviour {

    public TextFadeOut topText;
    public TextFadeOut itemText;
    public Text pressButtonText;
    private Dictionary<string, string> blas;
    private Dictionary<string, string> items;
    private Dictionary<string, string> pressButton;

    // Use this for initialization
    void Start () {
        blas = new Dictionary<string, string>();
        blas.Add("weapon", "It still feels dangerous to go alone...");
        blas.Add("tree", "With the right tool, I could surely smash that log!");
        blas.Add("missing", "No Text Available");
        blas.Add("firstLevel", "I hate swamps...");
        blas.Add("secondLevel", "I hate jungles...");
        blas.Add("thirdLevel", "I hate Deserts...");
        blas.Add("forthLevel", "Uhhh, I love Stones...");
        blas.Add("pond", "Not even deep enough, to drown myself...");
        blas.Add("dead", "Thats not how I wanted to die");
        blas.Add("hang1", "If I only had a rope");
        blas.Add("stone", "Hm.. Looks brittle");
        blas.Add("fireplace", "I cant even make fire..");
        blas.Add("toodark", "It's too dark over here, please let me out!");
        blas.Add("jar", "Perhaps I can catch some Fireflies");
        blas.Add("log", "Wood is Dead now... lucky wood");
        blas.Add("bridge", "I need something to build a bridge!");
        blas.Add("coal", "Thanks... that coal is useful as fuck.");
        blas.Add("firestone", "Perhaps a small junglefire helps?");
        blas.Add("sulfur", "Perhaps I should also eat some sulfur");
        blas.Add("rope", "A rope! Now lets find a nice tree");
        blas.Add("thinking", "Wait... That Big Stone.. Coal.. And Sulfur??");


        items = new Dictionary<string, string>();
        items.Add("missing", "No Text Available");
        items.Add("weapon", "Club");
        items.Add("log", "Log");
        items.Add("firestone", "FireStone");
        items.Add("jar", "Jar");
        items.Add("lightjar", "Lamp");
        items.Add("firstene", "Firestone");
        items.Add("coal", "Coal");
        items.Add("sulfur", "Sulfur");
        items.Add("rope", "Rope");
        items.Add("dynamite", "Dynamite");

        pressButton = new Dictionary<string, string>();
        pressButton.Add("missing", "No Text Available");
        pressButton.Add("log", "Press Space to place log");
        pressButton.Add("dynamite", "Press Space to combine sulfur and coal");
        pressButton.Add("jar", "Press Space to catch a Firefly");
        pressButton.Add("firestone", "Press Space to make fire");
        pressButton.Add("coal", "Press Space to make dynamite");
        pressButton.Add("sulfur", "Press Space to make dynamite");
        pressButton.Add("rope", "Pess Space to 'win'");

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void SetText(string text)
    {
        this.topText.SetText(text);
    }

    public void SetSpecialText(string text)
    {
        this.topText.SetText(blas[text]);
    }

    public void SetItemText(string text)
    {
        this.itemText.SetText("+ " + items[text]);
    }

    public void SetSpecialTextIfAvailable(string text)
    {
        this.topText.SetText(blas.ContainsKey(text) ? blas[text] : "");
    }

    public void SetItemRemovedText(string text)
    {
        this.itemText.SetText("- " + items[text]);
    }

    public void SetPressButtonTest(string text)
    {
        this.pressButtonText.text = pressButton[text];
    }
    public void RemovePressButtonTest()
    {
        this.pressButtonText.text = "";
    }

}
