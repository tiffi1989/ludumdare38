﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFadeOut : MonoBehaviour {

    private Text text;
    private float timer;
    public float showTime;
    public float fadeOutTime;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        timer += Time.deltaTime;
        if(timer < showTime)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
        }
        if(timer >= showTime && text.color.a > 0)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - .01f);

        }

    }

    public void SetText(string text)
    {
        this.text.text = text;
        timer = 0;
    }
}
