﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeScript : MonoBehaviour {

    private int counter;
    private Inventory inventory;
    private TalkScript talk;
    private Animator ani;
    private int blink = 10;
    private float blinkTimer;
    private SpriteRenderer renderer;
    private AudioSource audio;

    // Use this for initialization
    void Start () {
        this.inventory = GameObject.Find("Player").GetComponent<Inventory>();
        this.talk = GameObject.Find("Player").GetComponent<TalkScript>();
        this.ani = GetComponent<Animator>();
        this.renderer = GetComponent<SpriteRenderer>();
        this.audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        blinkTimer += Time.deltaTime;
        if (blink < 7 && blinkTimer > .1f)
        {
            blink++;
            renderer.color = renderer.color == Color.black ? Color.white : Color.black;
            blinkTimer = 0;
        }
        if (blink == 7)
        {
            renderer.color = Color.white;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(counter + " " + collision.gameObject.tag);

        if (collision.gameObject.tag == "Weapon")
        {
            blink = 0;
            audio.Play();
            
            if(counter++ > 3)
            {
                Break();
            }
        }
    }

    private void Break()
    {
        inventory.addItem("log");
        talk.SetItemText("log");
        ani.SetBool("Destroy", true);
        Invoke("kill", 1);
        renderer.color = Color.white;

    }

    private void kill()
    {
        Destroy(gameObject);

    }

}
