﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTextScript : MonoBehaviour {

    private bool alreadyActivated;
    public string text;
    public string itemNeeded;
    private Inventory inventory;
    public string skipWhenItem;

    // Use this for initialization
    void Start()
    {
        this.inventory = GameObject.Find("Player").GetComponent<Inventory>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            if(text != "" && !alreadyActivated && !(skipWhenItem != "" && inventory.hasPlayerItem(skipWhenItem)))
            {
                alreadyActivated = true;
                collision.gameObject.GetComponent<TalkScript>().SetSpecialText(text);
                if(itemNeeded == "")
                    GetComponent<BoxCollider2D>().enabled = false;
            }

            if(itemNeeded != "" && inventory.hasPlayerItem(itemNeeded))
            {
                collision.gameObject.GetComponent<TalkScript>().SetPressButtonTest(itemNeeded);
            }
        }       
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            collision.gameObject.GetComponent<TalkScript>().RemovePressButtonTest();
        }
    }
}
