﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            EnemyScript enemy = collision.gameObject.GetComponent<EnemyScript>();
            enemy.Hit();
        }

        if(collision.gameObject.tag == "Bat")
        {
            BatScript bat = collision.gameObject.GetComponent<BatScript>();
            bat.Hit();
        }

    }
}
